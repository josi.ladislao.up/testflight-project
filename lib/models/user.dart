import 'package:equatable/equatable.dart';

class User extends Equatable {
  final int id;
  final String name;
  final int age;
  final String type;
  final List<String> imageUrls;
  final String gender;
  final String bio;

  const User({
    required this.id,
    required this.name,
    required this.age,
    required this.type,
    required this.imageUrls,
    required this.gender,
    required this.bio,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        age,
        type,
        imageUrls,
        gender,
        bio,
      ];

  static List<User> users = [
    const User(
        id: 1,
        name: 'Lulu',
        age: 1,
        type: 'Siberian',
        imageUrls: [
          'https://encrypted-tbn3.gstatic.com/licensed-image?q=tbn:ANd9GcQVIYgxyZGmuHAmIdkk-ykz1Jkj-w2gADjrvoq4sDpaAQSJZmv00q6OdwC5vghUHxi5TgY7gD0INW29eOI'
        ],
        gender: 'Male',
        bio: 'None')
  ];
}
